/**
 * kc_linktracking.js
 *
 * Applique un trackevent au clic sur un lien
 *
 * Le template HTML à appliquer est le suivant :
 <a class="kc_linktracking more classes" data-eventCategory="click" data-eventAction="click_link" data-eventLabel="Link clicking" href='http://toto.com' rel="nofollow" target="_blank" type="" title="link title" >
    <p>Mon Lien</p>
 </a>
 *
 * Les valeurs par défaut des attributs si non renseignés sont
 * - dataEventCategory = "click";
 *
 * @copyright Kelcible 2017
 * @author Yann L'EBRELLEC
 * @date 23/03/2017
 */


(function($) { // on peut utiliser le $ de jQuery

    $(document).ready(function() { //Le DOM est chargé

        //Pour chaque span avec la classe 'kc_linktracking'
        $( '.kc_linktracking' ).each(function( index ) {

            var $this = $( this );

            //Récupération des informations dans les attributs
            var dataEventAction = $this.attr('data-eventAction');
            if (typeof dataEventAction === typeof undefined || dataEventAction === false) {
                dataEventAction = $this.attr("href");
            }
            var dataEventLabel = $this.attr('data-eventLabel');
            if (typeof dataEventLabel === typeof undefined || dataEventLabel === false) {
                dataEventLabel = $this.attr("href");
            }
            var dataEventCategory = $this.attr('data-eventCategory');
            if (typeof dataEventCategory === typeof undefined || dataEventCategory === false) {
                dataEventCategory = "click";
            }

           //On ajoute l'évenement au clic sur le lien de tracking
            $this.click(function( event ) {

                //Envoi à Google Analytics
                kcGaSend(dataEventCategory, dataEventAction, dataEventLabel);

            });

        });

    });

})(jQuery);
