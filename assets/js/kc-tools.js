/**
* kc_tools.js
*
* Fonctions génériques du module
*
* @copyright Kelcible 2017
* @author Yann L'EBRELLEC
* @date 23/10/2017
*/

/**
* Fonction d'envoi d'une action à google Analytics
* @param dataEventCategory category
* @param dataEventAction action
* @param dataEventLabel label
*/
function kcGaSend(dataEventCategory, dataEventAction, dataEventLabel){

  console.log('kcGaSend', dataEventCategory, dataEventAction, dataEventLabel);

  //Si ga est configuré
  if (typeof ga !== 'undefined') {
    ga('send', {
        hitType: 'event',
        eventCategory: dataEventCategory,
        eventAction: dataEventAction,
        eventLabel: dataEventLabel
    });
  }
  else
    //Si __gaTracker est configuré
    if (typeof __gaTracker !== 'undefined') {
        __gaTracker('send', {
            hitType: 'event',
            eventCategory: dataEventCategory,
            eventAction: dataEventAction,
            eventLabel: dataEventLabel
        });
    }
  else
    //si _gaq est configuré
    if (typeof _gaq !== 'undefined') {
        _gaq.push(['_trackEvent', dataEventAction, dataEventLabel]);
    }
}

/**
* Fonction d'envoi d'une page vue à google Analytics
* @param dataTitle Titre
* @param dataPage Page
*/
function kcPageview(dataTitle, dataPage){

  console.log('kcPageview', dataTitle, dataPage);

  //Si ga est configuré
  if (typeof ga !== 'undefined') {
    ga('set', 'title', dataTitle);
    ga('set', 'page', dataPage);
    ga('send', 'pageview');
  }
  else
    //Si __gaTracker est configuré
    if (typeof __gaTracker !== 'undefined') {
        __gaTracker('set', 'title', dataTitle);
        __gaTracker('set', 'page', dataPage);
        __gaTracker('send', 'pageview');
    }
  else
    //si _gaq est configuré
    if (typeof _gaq !== 'undefined') {
        _gaq.push(["_set", "title", dataTitle]);
        _gaq.push(["_set", "page", dataPage]);
        _gaq.push(["_trackPageview"]);
    }
}
