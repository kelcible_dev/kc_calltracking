/**
 * kc_buttonlink.js
 *
 * Permet de simuler un lien avec un bouton ou un autre élement
 *
 * Le template HTML à appliquer est le suivant :
 <span class="kc_buttonlink" data-href="/mentions-legales" ></span>
 <button class="kc_buttonlink" data-href="/mentions-legales" ></button>
 *
 * Les valeurs par défaut des attributs si non renseignés sont
 * - dataHref = "#"
 *
 * @copyright Kelcible 2016
 * @author Yann L'EBRELLEC
 * @date 02/06/2016
 */


(function($) { // on peut utiliser le $ de jQuery

    $(document).ready(function() { //Le DOM est chargé

        //Pour chaque span avec la classe 'kc_contacttracking'
        $( '.kc_buttonlink' ).each(function( index ) {

            var $this = $( this );

            var dataHref = $this.attr('data-href');

            //Récupération des informations dans les attributs
            $this.click(function($event){
                if ( !(typeof dataHref === typeof undefined || dataHref === false) ) {
                    location.href=dataHref;
                }
            });

        });

    });

})(jQuery);
