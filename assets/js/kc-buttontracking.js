/**
 * kc_buttontracking.js
 *
 * Applique un trackevent au clic sur un lien
 *
 * Le template HTML à appliquer est le suivant :
 <input type="submit" class="kc_buttontracking more classes" data-eventCategory="click" data-eventAction="click_button" data-eventLabel="Link clicking" name='submit-button' value="valider" />
 *
 * Les valeurs par défaut des attributs si non renseignés sont
 * - dataEventCategory = "click";
 *
 * @copyright Kelcible 2017
 * @author Yann L'EBRELLEC
 * @date 23/03/2017
 */


(function($) { // on peut utiliser le $ de jQuery

    $(document).ready(function() { //Le DOM est chargé

        //Pour chaque span avec la classe 'kc_buttontracking'
        $( '.kc_buttontracking' ).each(function( index ) {

            var $this = $( this );

            //Récupération des informations dans les attributs
            var dataEventAction = $this.attr('data-eventAction');
            if (typeof dataEventAction === typeof undefined || dataEventAction === false) {
                dataEventAction = $this.attr("name");
            }
            var dataEventLabel = $this.attr('data-eventLabel');
            if (typeof dataEventLabel === typeof undefined || dataEventLabel === false) {
                dataEventLabel = $this.attr("name");
            }
            var dataEventCategory = $this.attr('data-eventCategory');
            if (typeof dataEventCategory === typeof undefined || dataEventCategory === false) {
                dataEventCategory = "click";
            }

           //On ajoute l'évenement au clic sur le lien de tracking
            $this.click(function( event ) {

                //Envoi à Google Analytics
                kcGaSend(dataEventCategory, dataEventAction, dataEventLabel);

            });

        });

    });

})(jQuery);
