/**
 * kc_contacttracking.js
 *
 * Modifie le pageview à la presence d'une balise
 *
 * Le template HTML à appliquer est le suivant :
 <span class="kc_contacttracking" data-page="/contact-ok" data-title="Prise de contact par le formulaire"></span>
 *
 * Les valeurs par défaut des attributs si non renseignés sont
 * - dataPage = "/contact-ok"
 * - dataTitle = "Prise de contact par le formulaire";
 *
 * @copyright Kelcible 2016
 * @author Yann L'EBRELLEC
 * @date 02/06/2016
 */


(function($) { // on peut utiliser le $ de jQuery

    $(document).ready(function() { //Le DOM est chargé

        //Pour chaque span avec la classe 'kc_contacttracking'
        $( '.kc_contacttracking' ).each(function( index ) {

            var $this = $( this );

            //Récupération des informations dans les attributs
            var dataPage = $this.attr('data-page');
            if (typeof dataPage === typeof undefined || dataPage === false) {
                dataPage = "/contact-ok";
            }

            var dataTitle = $this.attr('data-title');
            if (typeof dataTitle === typeof undefined || dataTitle === false) {
                dataTitle = "Prise de contact par le formulaire";
            }

            //Modification des paramètres de visualisation de la page
            kcPageview(dataTitle, dataPage);

        });

    });

})(jQuery);
