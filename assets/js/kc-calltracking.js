/**
 * kc_calltracking.js
 *
 * Applique un trackevent au clic sur un lien "Appelez nous" pour afficher un numéro de téléphone
 *
 * Le template HTML à appliquer est le suivant :
 <span class="kc_calltracking" data-eventCategory="click" data-eventAction="telephone_display" data-eventLabel="Affichage telephone page contact">
    <a class="contact_link" >Appelez-nous !</a>
    <a class="contact_num" href="tel:+33xxxxxxx" >xx xx xx xx xx</a>
 </span>
 *
 * Les valeurs par défaut des attributs si non renseignés sont
 * - dataEventAction = "telephone_display"
 * - dataEventLabel = "Affichage telephone"
 * - dataEventCategory = "click";
 *
 * @copyright Kelcible 2016
 * @author Yann L'EBRELLEC
 * @date 31/05/2016
 */


(function($) { // on peut utiliser le $ de jQuery

    $(document).ready(function() { //Le DOM est chargé

        //Pour chaque span avec la classe 'kc_calltracking'
        $( '.kc_calltracking' ).each(function( index ) {

            var $this = $( this );

            //Récupération des éléments
            var $contact_num = $this.children("a.contact_num");
            var $contact_link = $this.children("a.contact_link");

            //Récupération des informations dans les attributs
            var dataEventAction = $this.attr('data-eventAction');
            if (typeof dataEventAction === typeof undefined || dataEventAction === false) {
                dataEventAction = "telephone_display";
            }
            var dataEventLabel = $this.attr('data-eventLabel');
            if (typeof dataEventLabel === typeof undefined || dataEventLabel === false) {
                dataEventLabel = "Affichage telephone";
            }
            var dataEventCategory = $this.attr('data-eventCategory');
            if (typeof dataEventCategory === typeof undefined || dataEventCategory === false) {
                dataEventCategory = "click";
            }

            //On cache le lien contenant le numéro
            $contact_num.hide();

            //On ajoute le pointer sur le lien de tracking
            $contact_link.css({cursor:'pointer'});

            //On ajoute l'évenement au clic sur le lien de tracking
            $contact_link.click(function( event ) {

                //Envoi à Google Analytics
                kcGaSend(dataEventCategory, dataEventAction, dataEventLabel);

                //On cache le lien de tracking puis on affiche celui contenant le numéro
                $contact_link.fadeOut('fast', function() {
                    $contact_num.fadeIn('slow');
                });

                //On annule l'action par défaut du lien de tracking
                event.preventDefault();
            });

        });

    });

})(jQuery);
