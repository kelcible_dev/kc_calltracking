/**
 * kc_mobilecall.js
 *
 * Permet de d'ajouter un bouton d'appel sur les devices de type mobile (déterminé par la méthode isMobilePhone)
 *
 * Le template HTML à appliquer est le suivant :
<div id="tel_mobile_fixed" data-eventcategory="click" data-eventaction="phonecall" data-eventlabel="Mobile Call Button">
    <a href="tel:+33000000000"><div class="tel_mobile_icon">Appeler</div></a>
</div>
 *
 * Les valeurs par défaut des attributs si non renseignés sont
 * data-eventaction="phonecall"
 * data-eventlabel="Mobile Call Button"
 *
 * @copyright Kelcible 2017
 * @author Yann L'EBRELLEC
 * @date 10/03/2017
 */


(function($) { // on peut utiliser le $ de jQuery

    $(document).ready(function() { //Le DOM est chargé

        //Modification CSS
        $("#tel_mobile_fixed").each(function(){
            $("#page.site").css('margin-bottom', '50px');
        });

        //Pour l'objet dont l'id est #tel_mobile_fixed
        $("#tel_mobile_fixed").click(function() {

            var $this = $( this );

            //Récupération des informations dans les attributs
            var dataEventAction = $this.attr('data-eventAction');
            if (typeof dataEventAction === typeof undefined || dataEventAction === false) {
                dataEventAction = "phonecall";
            }
            var dataEventLabel = $this.attr('data-eventLabel');
            if (typeof dataEventLabel === typeof undefined || dataEventLabel === false) {
                dataEventLabel = "Mobile Call Button";
            }
            var dataEventCategory = $this.attr('data-eventCategory');
            if (typeof dataEventCategory === typeof undefined || dataEventCategory === false) {
                dataEventCategory = "click";
            }

            //Envoi à Google Analytics
            kcGaSend(dataEventCategory, dataEventAction, dataEventLabel);

        });

    });

})(jQuery);
