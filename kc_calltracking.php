<?php
/*
Plugin Name: Kelcible CallTracking
Plugin URI: http://kelcible.fr
Description: Shortcodes simplifiés pour créer des actions SEO
Version: 1.1.2
Author: Yann L'EBRELLEC (Kelcible)
Author URI: http://kelcible.fr
License: GPL2
*/

include_once("classes/Tools.kc-calltracking.class.php");
include_once("classes/Shortcodes.kc-calltracking.class.php");

/**
 * Classe du module KC_calltracking_Plugin
 * @author Yann L'EBRELLEC
 */
class KC_calltracking_Plugin {

    /**
     * Version du module
     */
    private $kc_calltracking_version = '1.1.2';

    /**
     * Mode développement
     */
    private $mode_dev = false;

    /**
     * Constructeur du module
     */
    public function __construct() {

        //Initialisation des shortcodes
        new KC_calltracking_Shortcodes();

        //Initialisation des fichiers Javascript/css nécessaires
        add_action('wp_enqueue_scripts', array($this, 'kc_calltracking_init_js'));
        add_action('wp_enqueue_scripts', array($this, 'kc_calltracking_init_css'));

    }

    /**
     * Import des javascripts
     */
    public function kc_calltracking_init_js(){
        if(!$this->mode_dev){
            wp_enqueue_script ('kc_calltracking_js_all', plugins_url('/dist/kc-calltracking.min.js',__FILE__), array('jquery'), $this->kc_calltracking_version, false);
        }else{
            $arr_dev_js = array(
                'kc_tools_js' => '/js/kc-tools.js',
                'kc_calltracking_js' => '/js/kc-calltracking.js',
                'kc_contacttracking_js' => '/js/kc-contacttracking.js',
                'kc_buttonlink_js' => '/js/kc-buttonlink.js',
                'kc_mobilecall_js' => '/js/kc-mobilecall.js',
                'kc_linktracking_js' => '/js/kc-linktracking.js',
                'kc_buttontracking_js' => '/js/kc-buttontracking.js'
            );
            foreach($arr_dev_js as $js_key => $js_url){
                wp_enqueue_script ($js_key, plugins_url($js_url, __FILE__), array('jquery'), $this->kc_calltracking_version,   false);
            }
        }
    }

    /**
     * Import des css
     */
    public function kc_calltracking_init_css(){
        wp_enqueue_style ('kc_calltracking_css_all', plugins_url('/dist/kc-calltracking.min.css',__FILE__), array(), $this->kc_calltracking_version, 'all');
    }

}

//Instanciation du module
new KC_calltracking_Plugin();

?>
