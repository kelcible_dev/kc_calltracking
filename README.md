# Module WP kc_calltracking #

## Shortcode CALL-TRACKING ##

Dans les articles, page ou widget :
```
#!php

[kc_calltracking event-category='click' event-action='telephone_display' event-label='Affichage telephone' numero-label='00 00 00 00 00' numero-href='+33000000000' link-label='Appelez-nous !' classes='class1 class2']
```

Dans les fichiers PHP
```
#!php

echo do_shortcode( "[kc_calltracking event-category='click' event-action='telephone_display' event-label='Affichage telephone' numero-label='00 00 00 00 00' numero-href='+33000000000' link-label='Appelez-nous !' classes='class1 class2' ]" );
```


le shortcode génére le code html suivant : 

```
#!html

<span class="kc_calltracking class1 class2" data-eventcategory="click" data-eventaction="telephone_display" data-eventlabel="Affichage telephone">
    <a class="contact_link" >Appelez-nous !</a>
    <a class="contact_num" href="tel:+33000000000" >00 00 00 00 00</a>
</span>
```

## Shortcode CONTACT-TRACKING ##

Dans les articles, page ou widget :
```
#!php

[kc_contacttracking page='/contact-ok' title='Prise de contact par le formulaire']
```

Dans les fichiers PHP
```
#!php

echo do_shortcode( "[kc_contacttracking page='/contact-ok' title='Prise de contact par le formulaire']" ); 
```


le shortcode génére le code html suivant : 

```
#!html

<span class="kc_contacttracking" data-page="/contact-ok" data-title="Prise de contact par le formulaire"></span>
```

## Shortcode BUTTON-LINK ##

Dans les articles, page ou widget :
```
#!php

[kc_buttonlink href='/mentions-legales' classes='btn btn-default' type='button' label="Mentions Légales"]
```

Dans les fichiers PHP
```
#!php

echo do_shortcode( "[kc_buttonlink href='/mentions-legales' classes='btn btn-default' type='button' label='Mentions Légales']" );
```


le shortcode génére le code html suivant :

```
#!html

<button class="kc_buttonlink btn btn-default" data-href="/contact-ok" >Mentions Légales</button>

```

## Shortcode MOBILE-CALL ##

Dans les articles, page ou widget :
```
#!php

[kc_mobilecall event-category='click' event-action='phonecall' event-label='Mobile Call Button' numero-href='+33000000000' link-label='Appeler' classes='class1 class2']
```

Dans les fichiers PHP
```
#!php

echo do_shortcode( "[kc_mobilecall event-category='click' event-action='phonecall' event-label='Mobile Call Button' numero-href='+33000000000' link-label='Appeler'] classes='class1 class2' ]" );
```


le shortcode génére le code html suivant :

```
#!html

<div id="tel_mobile_fixed" class="class1 class2" data-eventcategory="click" data-eventaction="phonecall" data-eventlabel="Mobile Call Button">
    <a href="tel:+33000000000"><div class="tel_mobile_icon">Appeler</div></a>
</div>

```

## Shortcode LINK-TRACKING ##

Dans les articles, page ou widget :
```
#!php

[kc_linktracking event-category='click' event-action='click_link' event-label='Link clicking' href='http://toto.com' rel="nofollow" target="_blank" type="" title="Link Title" classes='more classes'] <p>Mon Lien</p> [/kc_linktracking]
```

Dans les fichiers PHP
```
#!php

echo do_shortcode( "[kc_linktracking event-category='click' event-action='click_link' event-label='Link clicking' href='http://toto.com' rel="nofollow" target="_blank" type="" title="Link Title" classes='more classes'] <p>Mon Lien</p> [/kc_linktracking]" );
```


le shortcode génére le code html suivant :

```
#!html

 <a class="kc_linktracking more classes" data-eventCategory="click" data-eventAction="click_link" data-eventLabel="Link clicking" href='http://toto.com' rel="" target="_blank" type="" title="link title">
    <p>Mon Lien</p>
 </a>

```

## Shortcode BUTTON-TRACKING ##

Dans les articles, page ou widget :
```
#!php

[kc_buttontracking event-category='click' event-action='click_link' event-label='Link clicking' value="OK" type="submit" classes='more classes']
```

Dans les fichiers PHP
```
#!php

echo do_shortcode( "[kc_buttontracking event-category='click' event-action='click_button' event-label='Button clicking' value='OK' name='submitButton' type="submit" classes='more classes']" );
```


le shortcode génére le code html suivant :

```
#!html

 <input type="submit" name="submitButton" class="kc_linktracking more classes" data-eventCategory="click" data-eventAction="click_button" data-eventLabel="Button clicking" value="OK" />

```


Comment changer le bouton d'envoi de commentaire sur les articles et les pages dans le fichier *www\wp-content\themes\<NOM_DU_THEME>\comments.php*

```
#!php

//https://developer.wordpress.org/reference/functions/comment_form/
$comment_args = array();
if ( shortcode_exists( 'kc_buttontracking' ) ) {
	$comment_args['submit_button'] = do_shortcode( '[kc_buttontracking event-category="click" event-action="submit_comment" event-label="'.get_the_title().'" name="%1$s" type="submit" id="%2$s" classes="%3$s" value="%4$s"]' );
}
comment_form($comment_args);
```

## Developpement ##

### Installation des dépendances ###

```
#!cmd

npm install

```

### Mode Développement ###

Dans le fichier `kc_calltracking.php`

```
#!php

$mode_dev = true;

```

Ce champ doit être absolument remis à `false` avant tout versionnement.

### Release / versionnement ###

Avant tout versionnement, il faut regénérer les fichiers du répertoire `dist/` avec la commande suivante :

```
#!cmd

grunt

```

Il faut obligatoirement repositionner à `false` le champ `$mode_dev` dans le fichier `kc_calltracking.php`

```
#!php

$mode_dev = false;

```

#### Minification des javascripts ####

La minification se fait avec `grunt-contrib-uglify`


```
#!cmd

grunt

```

#### Concaténation CSS/SASS ####

La minification se fait avec `grunt-contrib-sass`

Pour que cela fonctionne il faut avoir Ruby et Sass d'installé et que ce soit dans le PATH.

Pour que les CSS soient pris en compte, il faut lancer la commande suivante :

```
#!cmd

grunt

```

##### Ruby #####

http://rubyinstaller.org/

##### SASS #####

```

gem install sass

```


## Changelogs ##

* 1.1.2 | Création du fichier kc-tools pour mutualiser le code
* 1.1.1 | Ajout du shortcode button-tracking
* 1.1.0 | Configuration du __gaTracker pour MonsterInsights
* 1.0.9 | Ajout du shortcode link-tracking
* 1.0.8 | Création de classes Tools et Shortcodes et déplacement JS/CSS dans un dossier assets
* 1.0.7 | Ajout de classes CSS possible sur les shortcodes calltracking et mobilecall
* 1.0.6 | Utilisation de Sass pour les fichiers CSS
* 1.0.5 | Minification des fichiers JS
* 1.0.4 | Ajout du shortcode mobile-call
* 1.0.3 | Ajout du shortcode button-link
* 1.0.2 | Ajout du shortcode contact-tracking
* 1.0.1 | Ajout du shortcode call-tracking
