<?php

/**
 * Classe des shortcodes KC_calltracking_Shortcodes
 * @author Yann L'EBRELLEC
 */
class KC_calltracking_Shortcodes {

     protected $print_shortcode = false;

    /**
     * Constructeur
     */
    public function __construct() {
        //Initialisation des shortcodes
        add_shortcode( 'kc_calltracking', array($this,'kc_calltracking_shortcode') );
        add_shortcode( 'kc_contacttracking', array($this,'kc_contacttracking_shortcode') );
        add_shortcode( 'kc_buttonlink', array($this,'kc_buttonlink_shortcode') );
        add_shortcode( 'kc_mobilecall', array($this,'kc_mobilecall_shortcode') );
        add_shortcode( 'kc_linktracking', array($this,'kc_linktracking_shortcode') );
        add_shortcode( 'kc_buttontracking', array($this,'kc_buttontracking_shortcode') );
    }

    /**
     * Création du shortcode call-tracking
     * [kc_calltracking event-category='click' event-action='telephone_display' event-label='Affichage telephone' numero-label='00 00 00 00 00' numero-href='+33000000000' link-label='Appelez-nous !' classes='more classes']
     */
    public function kc_calltracking_shortcode($atts){

        $html_return = "";

        $a = shortcode_atts( array(
            'event-category' => 'click',
            'event-action' => 'telephone_display',
            'event-label' => 'Affichage telephone',
            'numero-label' => '00 00 00 00 00',
            'numero-href' => '+33000000000',
            'link-label' => 'Appelez-nous !',
            'classes' => ''
        ), $atts );

        $html_return = '
<span class="kc_calltracking '.$a['classes'].'"
      data-eventCategory="'.$a['event-category'].'"
      data-eventAction="'.$a['event-action'].'"
      data-eventLabel="'.$a['event-label'].'">
    <a class="contact_link" >'.$a['link-label'].'</a>
    <a class="contact_num" href="tel:'.$a['numero-href'].'" style="display:none;">'.$a['numero-label'].'</a>
</span>';

        return $html_return;

    }

    /**
     * Création du shortcode contact-tracking
     * [kc_contacttracking page='/contact-ok' title='Prise de contact par le formulaire' ]
     */
    public function kc_contacttracking_shortcode($atts){

        $html_return = "";

        $a = shortcode_atts( array(
            'title' => 'Prise de contact par le formulaire',
            'page' => '/contact-ok'
        ), $atts );

        $html_return = '
<span class="kc_contacttracking" data-page="'.$a['page'].'" data-title="'.$a['title'].'"></span>';

        return $html_return;

    }

    /**
     * Création du shortcode button-link
     * [kc_buttonlink href='/mentions-legales' classes='btn' type='button' label="Mentions Légales"]
     */
    public function kc_buttonlink_shortcode($atts){

        $html_return = "";

        $a = shortcode_atts( array(
            'href' => '#',
            'classes' => '',
            'type' => 'button',
            'label' => 'Lien'
        ), $atts );

        $html_return = '
<'.$a['type'].' class="kc_buttonlink '.$a['classes'].'" data-href="'.$a['href'].'" >'.$a['label'].'</'.$a['type'].'>';

        return $html_return;

    }

    /**
     * Création du shortcode mobile-call
     * [kc_mobilecall event-category='click' event-action='phonecall' event-label='Mobile Call Button' numero-href='+33000000000' link-label='Appeler' classes='more classes']
     */
    public function kc_mobilecall_shortcode($atts){

        $html_return = "";

        $a = shortcode_atts( array(
            'event-category' => 'click',
            'event-action' => 'telephone_display',
            'event-label' => 'Affichage telephone',
            'numero-href' => '',
            'link-label' => '',
            'classes' => ''
        ), $atts );

        if(KC_calltracking_Tools::isMobilePhone() && !empty($a['numero-href'])){

            $html_return = '<div id="tel_mobile_fixed" class="'.$a['classes'].'"
            data-eventCategory="'.$a['event-category'].'"
            data-eventAction="'.$a['event-action'].'"
            data-eventLabel="'.$a['event-label'].'">
                <a href="tel:'.$a['numero-href'].'">
                    <div class="tel_mobile_icon">'.$a['link-label'].'</div>
                </a>
            </div>';

        }

        return $html_return;

    }

     /**
     * Création du shortcode link-tracking
     * [kc_linktracking event-category='click' event-action='click_link' event-label='Link clicking' href='http://toto.com' rel="nofollow" target="_blank" type="" title="link title" classes='more classes'] <p>Mon Lien</p> [/kc_linktracking]
     */
    public function kc_linktracking_shortcode($atts, $content = null){

        $html_return = "";

        $a = shortcode_atts( array(
            'event-category' => 'click',
            'event-action' => '',
            'event-label' => '',
            'href' => '',
            'rel' => '',
            'target' => '',
            'type' => '',
            'title' => '',
            'classes' => ''
        ), $atts );

        if($a['href'] === ''){
            return '<div>L\'attribut [href] n\'est pas renseigné</div>';
        }

        if($a['event-label'] === ''){
            return '<div>L\'attribut [event-label] n\'est pas renseigné</div>';
        }

        if($a['title'] === ''){
            $a['title'] === $a['event-label'];
        }

        $unrequired_atts = '';
        $unrequired_atts .= ($a['rel']!=='')?' rel="'.$a['rel'].'" ':'';
        $unrequired_atts .= ($a['target']!=='')?' target="'.$a['target'].'" ':'';
        $unrequired_atts .= ($a['type']!=='')?' type="'.$a['type'].'" ':'';

        $html_return = '
<a class="kc_linktracking '.$a['classes'].'"
      data-eventCategory="'.$a['event-category'].'"
      data-eventAction="'.$a['event-action'].'"
      data-eventLabel="'.$a['event-label'].'"
      href="'.$a['href'].'"
      title="'.$a['title'].'"
      '.$unrequired_atts.'
      >'.html_entity_decode ($content).'
</a>';

        return $html_return;

    }

         /**
     * Création du shortcode button-tracking
     * [kc_buttontracking event-category='click' event-action='click_link' event-label='Link clicking' type='submit' name='submit-button' value='Valider' classes='more classes']
     */
    public function kc_buttontracking_shortcode($atts, $content = null){

        $html_return = "";

        $a = shortcode_atts( array(
            'event-category' => 'click',
            'event-action' => '',
            'event-label' => '',
            'type' => 'button',
            'value' => '',
            'name' => '',
            'disabled' => '',
            'form' => '',
            'formaction' => '',
            'formenctype' => '',
            'formmethod' => '',
            'formtarget' => '',
            'classes' => ''
        ), $atts );

        if($a['value'] === ''){
            return '<div>L\'attribut [value] n\'est pas renseigné</div>';
        }

        if($a['name'] === ''){
            return '<div>L\'attribut [name] n\'est pas renseigné</div>';
        }

        if($a['event-label'] === ''){
            return '<div>L\'attribut [event-label] n\'est pas renseigné</div>';
        }

        $unrequired_atts = '';
        $unrequired_atts .= ($a['disabled']==='disabled')?' disabled="disabled" ':'';
        $unrequired_atts .= ($a['form']!=='')?' form="'.$a['form'].'" ':'';
        $unrequired_atts .= ($a['formaction']!=='')?' formaction="'.$a['formaction'].'" ':'';
        $unrequired_atts .= ($a['formenctype']!=='')?' formenctype="'.$a['formenctype'].'" ':'';
        $unrequired_atts .= ($a['formmethod']!=='')?' formmethod="'.$a['formmethod'].'" ':'';
        $unrequired_atts .= ($a['formtarget']!=='')?' formtarget="'.$a['formtarget'].'" ':'';

        $html_return = '
<input class="kc_buttontracking '.$a['classes'].'"
      data-eventCategory="'.$a['event-category'].'"
      data-eventAction="'.$a['event-action'].'"
      data-eventLabel="'.$a['event-label'].'"
      type="'.$a['type'].'"
      name="'.$a['name'].'"
      value="'.$a['value'].'"
      '.$unrequired_atts.'
      />';

        return $html_return;

    }

     /**
     * Affichage du shortcode
     * @author Yann L'EBRELLEC
     */
    protected function printShortCode($sc_name, $a){

        if(!$this->print_shortcode){
            return;
        }

        $str_sc = '['.$sc_name;
        foreach($a as $a_k => $a_v){
            if(!empty($a_v)){
                $str_sc .= ' '.$a_k.'="'.$a_v.'"';
            }
        }
        $str_sc .= ']';
        return "<pre>".$str_sc."</pre>";
    }

}

?>
